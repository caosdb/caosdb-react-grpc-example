# Prerequisites

 * We assume a Linux OS and Bash
 * A Browser
 * Git
 * Node Package Manager npm (tested version: 6.14.15)
 * ProtoBuf compiler (tested version: 3.17.1) - `protoc` is expected to be
   on your `PATH`.
 * Grpc-Web plugin (https://github.com/grpc/grpc-web/releases/tag/1.3.0) - Just
   add that `protoc-gen-grpc-web` executable to your `PATH`
 * A CaosDB Server (https://gitlab.com/caosdb/caosdb-server) >= v0.6.0
 * Docker or Envoy 1.20 (https://envoyproxy.io)

# Setup React App

1. Create a react app
   `npx create-react-app caosdb-extension-example`
2. Clone caosdb-proto repo
   `git clone https://gitlab.com/caosdb/caosdb-proto`
3. Generate the GRPC-Client code with the bash script (or whatever would be
   right on your OS)
   `./generate_sources.sh`. If the `protoc` or `protoc-gen-grpc-web`
   executables are not on you path, export the environment variables `PROTOC`
   or `GRPCWEB_PLUGIN` respectively. The script prints the paths to the
   respective binaries and should tell you that it generated
   `proto/caosdb/entity/v1` and `proto/caosdb/info/v1`.
4. Go to react directory (e.g. `./caosdb-extension-example/`) and install
   grpc-dependencies:
   `npm install --save google-protobuf grpc-web`
5. In the react-directoy: Copy our example code into the src directory:
   `cp ../App.js src/`
6. Add `"homepage": "ext",` to `package.json` (in the top level object, e.g.
   under `name`.
7. Start the development environment: `npm start`. Your can browse to the react
   app in your browser (`localhost:3000/ext` or whatever `npm start` says).

You will see something along the lines of `An error occured: Unknown
Content-type received.` (cf. [error.png](./error.png)) -- That is fine for now.
The react app is working. We have to setup the server and the envoy proxy now.

# Start the CaosDB Server

1. Configure a CaosDB Server (>=0.6.0) with anonymous sessions enabled. There
   are several ways how to do this, depending on how you deploy the server.

   If you use a setup with LinkAhead/Docker, the `profile.yaml` should
   have `conf.network.port_grpc_plain: 8080`
   and `conf.server.conf.auth_optional: TRUE`.
   Also, make sure you are using the version of the linkahead script which is
   in the latest dev branch.

   Otherwise, running a server directly from the caosdb-server repository: Put
   these options into one of your `server.conf` files: `AUTH_OPTIONAL=TRUE` and
   `GRPC_SERVER_PORT_HTTP=8080`.
   See [the server documentation](https://docs.indiscale.com/caosdb-server/administration/configuration.html)
   for more info about this second way.

2. Start the CaosDB Server.

   With Linkahead/Docker: `./linkahead -p profile.yml start --no-health`

   From the caosdb-server repository: `make run`

3. Browse to the webinterface to be sure that anonymous sessions are activated:
   `https://localhost:10443` (Click away warings about the self-signed certificate if
   necessary). When it says "Please log in" the authentication is still
   required and this is not suitable for our small example. Otherwise, a
   default welcome page indicates that everything is ok.

# Configure the Proxy

We need a proxy for the whole setup to work for two reasons:

1. The proxy has to map from HTTP1 (Web-)GRPC requests to HTTP2 GRPC requests
   and vice versa.
2. The proxy shall serve the react app and the other services under the same
   port and domain (otherwise xss-policies of modern browsers will likely
   prevent the setup to work).

We are using Envoy, because it can to configured to do the mapping very easily
and there are currently no other proxies around that support web-grpc by
default (at least to my knowledge) .

So start an envoy server using the `envoy.yaml`. The easiest way to do this is
via Docker:
`docker run -p 8081:8081 -v $(pwd)/envoy.yaml:/etc/envoy/envoy.yaml --network host envoyproxy/envoy:v1.20.0`

You may have to adapt the path to the `envoy.yml` in the above command.

Note: `--network host` is considered dangerous and should not be used in
production environments. In production you would either start envoy using a
dedicated docker network or deploy envoy without docker.

# Ready

Now browse to `http://localhost:8081` (no https! in this setup) and find the default web interaface
there. Under `http://locahost:8081/ext` you will find the react app you just
created and you should see that the server's version info is being loaded and
printed (cf. [loading.png](./loading.png) and [success.png](./success.png)).

That's it. You used the CaosDB GRPC-API in a React App.

By the way: `create-react-app` also initialized a git repository under
`caosdb-extension-example/`. So if you want to start developing an actual
extension for the CaosDB web interface, you can start right there.

For developing a react.js app, please have a look into react's
[documentation](https://reactjs.org/docs/getting-started.html) and
[tutorials](https://reactjs.org/tutorial/tutorial.html). 

In `caosdb-extension-example/src/generated/proto/caosdb/{entity,info}/v1` you
find two files: `main_grpc_web_pb.js` and `main_pb.js`. The former contains the
(auto-generated) implemntation of the services of CaosDB's GRPC API and is the
one you use in your custom code (see the first lines of [App.js](App.js)). The
latter contains the implementation of the GRPC message objects used by the
API. For developing your own CaosDB extension it may help to have a look at both
files to see how the sevices and the message objects are defined and how they
can be accessed.
