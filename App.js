import React, { useState } from 'react';

const proto = {};
proto.caosdb = {};
proto.caosdb.info = {};
proto.caosdb.info.v1 = require('./generated/proto/caosdb/info/v1/main_grpc_web_pb.js');
const info_api = proto.caosdb.info.v1;

export default function App() {
  const [version_info, set_version_info] = useState(null);

  if(version_info === null) {
    // first time this App is mounted.
    set_version_info("Loading...");

    const client = new info_api.GeneralInfoServiceClient("/api", null, null);
    const request = new info_api.GetVersionInfoRequest();

    client.getVersionInfo(request, {}, (error_response, response) => {
      if(error_response) {
        // show error
        set_version_info(`An error occured: ${error_response.message}`);
      } else {
        // show servers version
        const major = response.getVersionInfo().getMajor();
        const minor = response.getVersionInfo().getMinor();
        const patch = response.getVersionInfo().getPatch();
        const pre_release = response.getVersionInfo().getPreRelease();

        var version_string = `v${major}.${minor}.${patch}`;
        if(pre_release)
          version_string += `-${pre_release}`;

        set_version_info(`CaosDB Server Version: ${version_string}`);
      }
    });
  }

  return (<div><h1>CaosDB Webui Extension Example</h1><div>{version_info}</div></div>);
}
